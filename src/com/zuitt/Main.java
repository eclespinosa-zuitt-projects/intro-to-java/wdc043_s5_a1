package com.zuitt;

public class Main {
    public static void main(String[] args) {
        Contact person1 = new Contact("John", "09123456789", "XYZ St.");
        person1.setAddresses("ABC St.");
        person1.setNumbers("09987654321");

        Contact person2 = new Contact("Paul", "+639789123456", "ASD St.");
        person2.setNumbers("+639123789456");
        person2.setAddresses("QWE St.");

        Phonebook myPhonebook = new Phonebook();
        myPhonebook.setContacts(person1);
        myPhonebook.setContacts(person2);

        printContact(myPhonebook);

    }

    private static void printContact(Phonebook phonebook) {
        if(phonebook.getContacts().isEmpty()) {
            System.out.println("IMPORTANT! You have no contacts in your phonebook.");
        } else {
            for (Contact contact: phonebook.getContacts()) {
                print(contact);
            }
        }
    }

    private static void print(Contact contact) {
        System.out.println(contact.getName());
        System.out.println("--------------------------");

        System.out.println(contact.getName() + " has the following registered numbers:");
        for(String num: contact.getNumbers()) {
            System.out.println(num);
        }

        System.out.println("--------------------------");

        System.out.println(contact.getName() + " has the following registered addresses:");
        for(String address: contact.getAddresses()) {
            System.out.println(address);
        }

        System.out.println("=============================");
    }
}
