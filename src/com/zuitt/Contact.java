package com.zuitt;

import java.util.ArrayList;

public class Contact {
    private String name;
    private ArrayList<String> numbers = new ArrayList<>();
    private ArrayList<String> addresses = new ArrayList<>();

    public Contact() {
    }

    public Contact(String name, String number, String address) {
        this.name = name;
        this.numbers.add(number);
        this.addresses.add(address);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<String> getNumbers() {
        return numbers;
    }

    public ArrayList<String> getAddresses() {
        return addresses;
    }

    public void setNumbers(String number) {
        this.numbers.add(number);
    }

    public void setAddresses(String address) {
        this.addresses.add(address);
    }
}
